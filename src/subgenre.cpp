#include "../header/subgenre.h"
#include <iostream>

using namespace std;

subgenre::subgenre():
  subgenre(0, "")
{}

subgenre::subgenre(int _id, string _sub_type) :
  id(_id),
  sub_type(_sub_type)
{}

int subgenre::getId()
{
  return id;
}

string subgenre::getSub_type()
{
  return sub_type;
}

void subgenre::setSub_type(string _sub_type)
{
  sub_type = _sub_type;
}
