#include "../header/genre.h"
#include <iostream>

using namespace std;

genre::genre():
  genre(0, "")
{}

genre::genre(int _id, string _type):
  id(_id),
  type(_type)
{}

int genre::getId()
{
  return id;
}

string genre::getType()
{
  return type;
}

void genre::setType(string _type)
{
  type = _type;
}
