#include "../header/artist.h"
#include <iostream>

using namespace std;

artist::artist():
  artist(0, "")
{}

artist::artist(int _id, string _name) :
  id(_id),
  name(_name)
{}

int artist::getId()
{
  return id;
}

string artist::getName()
{
  return name;
}

void artist::setName(string _name)
{
  name = _name;
}
