#include "../header/format.h"
#include <iostream>

using namespace std;

format::format():
  format(0, "")
{}

format::format(int _id, string _type) :
  id(_id),
  type(_type)
{}

int format::getId()
{
  return id;
}

string format::getType()
{
  return type;
}

void format::setType(string _type)
{
  type = _type;
}
