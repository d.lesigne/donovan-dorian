#ifndef POLYPHONY_H
#define POLYPHONY_H
#include <iostream>
using namespace std;

/**
 * \class polyphony
 * \brief definition of the class polyphony
 */
class polyphony
{
 public:

  /**
   * \brief default constructor of polyphony
   */
  polyphony();

  /**
   * \brief constructor with parameters of polyphony
   * \param id the identifier of a polyphony
   * \param number the number of a polyphony
   */
  polyphony(int, int);

  /**
   * \brief allows reading an identifier of a polyphony
   * \return int representing the id of a polyphony
   */
  int getId();

  /**
   * \brief allows reading a number of a polyphony
   * \return int representing the number of a polyphony
   */
  int getNumber();

  /**
   * \brief allows writting a number of a polyphony
   */
  void setNumber(int);
 private:
  int id;///< Attribute defining the id of a polyphony
  int number;///< Attribute defining the number of a polyphony
};

#endif // POLYPHONY_H
