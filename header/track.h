#ifndef TRACK_H
#define TRACK_H
#include <iostream>
#include <vector>
#include "format.h"
#include "genre.h"
#include "subgenre.h"
#include "polyphony.h"
#include "artist.h"
#include "album.h"

using namespace std;

/**
 * \class track
 * \brief Definition of the class track
 */
class track
{
 public:
  /**
   * \brief default constructor of track
   */
  track();
  /**
   * \brief constructor with parameters of track
   * \param id the identifier of a track
   * \param name the name of a track
   * \param path the path of a track
   * \param duration the duration of a track
   * \param vector<artist> the list of artist for a track
   * \param vector<album> the list of album for a track
   * \param format the format of a track
   * \param genre the genre of a track
   * \param subgenre the subgenre of a track
   * \param polyphony the polyphony of a track
   */
  track(int, string, string, int, vector<artist>, vector<album>, format, genre, subgenre, polyphony);

  /**
   * \brief allows reading a identifier of a track
   * \return int representing the id of a track
   */
  int getId();

  /**
   * \brief allows reading a name of a track
   * \return String representing the name of a track
   */
  string getName();

  /**
   * \brief to set the name of a track
   */
  void setName(string);

  /**
   * \brief allows reading a path of a track
   * \return String representing the path of a track
   */
  string getPath();

  /**
   * \brief to set the path of a track
   */
  void setPath(string);

  /**
   * \brief allows reading a duration of a track
   * \return int representing the duration of a track
   */
  int getDuration();

  /**
   * \brief to set the duration of a track
   */
  void setDuration(int);

  /**
   * \brief allows reading an artist of the list of artist
   * \return artist type in reference to the artist class
   */
  vector<artist> getArtists();

  /**
   * \brief allows reading an album of the list of album
   * \return album type in reference to the album class
   */
  vector<album> getAlbums();

  /**
   * \brief allows reading a format of a track
   * \return format representing the format of a track
   */
  format getFormat();

  /**
   * \brief to set the format of a track
   */
  void setFormat(format);

  /**
   * \brief allows reading a genre of a track
   * \return genre representing the genre of a track
   */
  genre getGenre();

  /**
   * \brief to set the genre of a track
   */
  void setGenre(genre);

  /**
   * \brief allows reading a subgenre of a track
   * \return genre representing the subgenre of a track
   */
  subgenre getSubgenre();

  /**
   * \brief to set the subgenre of a track
   */
  void setSubgenre(subgenre);

  /**
   * \brief allows reading a polyphony of a track
   * \return genre representing the polyphony of a track
   */
  polyphony getPolyphony();

  /**
   * \brief to set the polyphony of a track
   */
  void setPolyphony(polyphony);

 private:
  int id;///< Attribute defining the id of a track
  string name;///< Attribute defining the name of a track
  string path;///< Attribute defining the path of a track
  int duration;///< Attribute defining the duration of a track
  vector<artist> artists;///< Attribute defining artists of a track
  vector<album> albums;///< Attribute defining albums of a track
  format Format;///< Attribute defining the format of a track
  genre Genre;///< Attribute defining the genre of a track
  subgenre Subgenre;///< Attribute defining the subgenre of a track
  polyphony Polyphony;///< Attribute defining the polyphony of a track
};
#endif // TRACK_H
